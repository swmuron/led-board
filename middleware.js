var express = require('express');

// configure middleware
module.exports = function(app) {

  // serve public files
  app.use(express.static(__dirname + '/public'));
  app.use(function(req, res, next) {
  req.rawBody = '';
  //req.setEncoding('utf8');

  req.on('data', function(chunk) { 
    req.rawBody += chunk;
  });

  req.on('end', function() {
    try {
      req.body = JSON.parse(req.rawBody);
    } catch (e) {
      //console.log('bad json body',e);
      req.body = req.rawBody;
    }
    next();
  });
});
  //app.use(bodyParser.json());
  // logger?
}