// Express modules
var express = require('express');

// Other modules
var config = require('./config/config');
// App, config, definition
var app = express();

// set up DB connection

// middleware
require('./middleware')(app);
// auto routers
require('./setupRoutes')(app);

// Poll cloud server
require('./lib/sources/cloud.js')();


app.listen(config.HTTP.PORT);
console.log('Running with config',config.ENV_NAME)