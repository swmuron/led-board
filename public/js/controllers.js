var DisplayController = function($scope, Display, $interval, $timeout) {
  $scope.currentScreen = 0;
  $scope.postCount = 0;
  $scope.showingPost = false;
  // hashtag
  $scope.hashtag = 'IronyLLC'
  function getNextDisplay() {
    Display.getDisplayPost().then(function(data) {
      $scope.post = data;
      console.log(data);
      if (!data.imageUrl) {
        $scope.post.imageUrl = 'img/default.png';
      }
      // set size based on username length
      switch ($scope.post.username.length) {
        case 30:
        case 29:
        case 28:
          $scope.userstyle = "font-size: 9px;";
          break;
        case 27:
        case 26:
        case 25:
        case 24:
          $scope.userstyle = "font-size: 11px;";
          break;
        case 23:
        case 22:
          $scope.userstyle = "font-size: 12px;";
          break;
        case 21:
        case 20:
        case 19:
          $scope.userstyle = "font-size: 14px;";
          break;
        case 18:
          $scope.userstyle = "font-size: 15px;";
          break;
        case 17:
          $scope.userstyle = "font-size: 16px;";
          break;
        case 16:
          $scope.userstyle = "font-size: 17px;";
          break;
        case 15:
        case 14:
          $scope.userstyle = "font-size: 18px;";
          break;
        case 13:
          $scope.userstyle = "font-size: 20px;";
          break;
        case 12:
          $scope.userstyle = "font-size: 22px;";
          break;
        case 11:
          $scope.userstyle = "font-size: 24px;";
          break;
        case 10:
          $scope.userstyle = "font-size: 26px;";
          break;
        case 9:
          $scope.userstyle = "font-size: 28px;";
          break;
        default:
          $scope.userstyle = "font-size: 30px;";
          break;
      }
      $scope.showingPost = true;
    });  
  }
  getNextDisplay();

  function getLeaderboard() {
    console.log('getlb');
    Display.getLeaderboard().then(function(data) {
      console.log('getlb data',data);
      $scope.leaderboard = data;
    })
  }
  getLeaderboard();
  

  $interval(function() {
    console.log('yo',$scope.currentScreen,$scope.postCount);

    //$scope.postCount = 1;
    //$scope.currentScreen = 1;
    //return; // DEBUG FOR LBOARD
    if ($scope.currentScreen == 0 && $scope.postCount < 12) {
      // cycle post
      $scope.postCount += 1;
      $scope.showingPost = false;
      $timeout(getNextDisplay, 500);
      getLeaderboard();
    } else if ($scope.currentScreen > 0 && $scope.postCount < 3) {
      $scope.postCount += 1;
    } else {
      $scope.postCount = 0;
      if ($scope.currentScreen >= 2 ) {
        $scope.currentScreen = 0;
        document.getElementById('theVideo').pause();
      } else {
        $scope.currentScreen++;
        if ($scope.currentScreen == 2) {
          // restart video
          var v = document.getElementById('theVideo');
          v.currentTime = 0;
          v.play();
        }
      }
    }
    
  }, 2500);
}