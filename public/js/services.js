var depotServices = angular.module('hdepotServices', []);

depotServices.factory('Post', function($http, $q, $rootScope) {
  var PostService = {};

  PostService.getQueue = function() {
    var deferred = $q.defer();

    $http.get('/api/queue').success(function(data) {
      for (var i in data) {
        console.log(i,data);
        var d = data[i].value.split(';');
        data[i].username = d[0];
        data[i].text = d[1];
        data[i].imageUrl = d[2];
        if (!data[i].imageUrl) {
          data[i].imageUrl = 'img/default.png';
        }
      }
      deferred.resolve(data);
    });

    return deferred.promise;
  }

  PostService.acceptPost = function(postId) {
    var deferred = $q.defer();

    $http.post('/api/queue/accept', {id: postId}).success(function(data) {
      deferred.resolve(true);
    }).error(function(data) {
      deferred.reject(false);
    });

    return deferred.promise;
  }

  PostService.rejectPost = function(postId) {
    var deferred = $q.defer();

    $http.post('/api/queue/reject', {id: postId}).success(function(data) {
      deferred.resolve(true);
    }).error(function(data) {
      deferred.resolve(false);
    });

    return deferred.promise;
  }

  PostService.banPost = function(postId) {
    var deferred = $q.defer();

    deferred.reject('unimplemented');

    return deferred.promise;
  }

  return PostService;
});

depotServices.factory('Display', function($http, $q, $rootScope) {
  var DisplayService = {};

  DisplayService.getDisplayPost = function() {
    var deferred = $q.defer();

    $http.get('/api/display', {cache: false}).success(function(data) {
      console.log(data);
      for (var i in data) {
        var d = data[i].value.split(';');
        data[i].username = d[0];
        data[i].text = d[1];
        data[i].imageUrl = d[2];
        if (!data[i].imageUrl) {
          data[i].imageUrl = 'img/default.png';
        }
      }
      deferred.resolve(data[0]);
    });

    return deferred.promise;
  }

  DisplayService.getLeaderboard = function() {
    var deferred = $q.defer();

    $http.get('/api/display/leaderboard').success(function(data) {
      //console.log('resp',data); 
      deferred.resolve(data);
    });

    return deferred.promise;
  }

  return DisplayService;
});