
var Post = require('../models/post.js'),
    express = require('express');

var DisplayRouter = function() {
  var r = express.Router();
  
  r.get('/', function(req,res) {
    // get queue list
    //var pageKey = req.query.after || null;
    //console.log('getqueue');
    Post.getDisplay().then(function(data) {
      //console.log('got next display',data);
      return res.send(data.rows);
    }, function(err) {
      console.log('getdisplay failed');
      return res.send(500);
    });
  });

  r.get('/leaderboard', function(req,res) {
    //console.log('hit leaderboard')
    Post.getLeaderboard().then(function(rows) {
      //console.log('lboard',rows);
      return res.send(rows);
    })
  });

  return r;
}

module.exports = {
  path: 'display',
  router: DisplayRouter()
}