// Post model helpers
// handle CRUD functions

var cfg = require('../../config/config.js');
//var q   = require('q');
var fs = require('fs');

var request = require('request');
var path = require('path'),
    appDir = path.dirname(require.main.filename);
var Cot = require('cot');

if (!global._db) {
  global._db = (new Cot({'port': cfg.DB.PORT, 'hostname': cfg.DB.HOST})).db(cfg.DB.NAME);
}

var Post = { };

var regexExt = /\.\w{3,4}$/gi;

var regexUrl = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/gi;
var regexTco = /http:\/\/t.co\/\w*/gi;

function stripURLs(str) {
  var i = str.replace(regexUrl,'');
  var ii = i.replace(regexTco,'');
  console.log(str,' > ',i,' >',ii);
  return ii;
}

Post.createFromCloud = function(src) {
  var doc = src.value;


  downloadFile('http://'+cfg.UPSTREAM.HOST+':'+cfg.UPSTREAM.PORT+doc.imageUrl,appDir+'/public'+doc.imageUrl);

  return Post.save(doc);
}

Post.getById = function(id) {
  return global._db.get(id);
}

Post.updateStatus = function(id, status) {
  // thus here, update the user leaderboard
  return global._db.update(id, function(doc) {
    doc.status = status;
    return doc;
  });
}

global.maxViewCount = 0;
global._db.view('post', 'display', {limit: 1, descending: true}).then(function(data) {
  global.maxViewCount = data.rows[0].key[0];
});
Post.updateViewCount = function(id) {
  return global._db.update(id, function(doc) {
    if (doc.viewCount+1 > global.maxViewCount) {
      global.maxViewCount = doc.viewCount+1;
    }
    doc.viewCount = global.maxViewCount;
    return doc;
  });
}

Post.getDisplay = function() {
  // TODO: hammer down exact logic

  // if an unviewed post exists, display that
  return global._db.view('post', 'display', {limit: 1, descending: false}).then(function(data) {
    var post = data.rows[0];
    //console.log('got post',post);

    Post.updateViewCount(post.id).then(function(data) {
      //console.log('updated view count',post.id);
    });
    return data;
  });
  // otherwise ??

}

Post.getLeaderboard = function(id) {
  return global._db.view('post', 'leaderboard', {group: true}).then(function(data) {
    data.rows.sort(function(a, b) {
        return b.value - a.value;
    });

    return data.rows.slice(0,5);
  });
}

Post.save = function(u) {
  return global._db.post(u); 
}

function downloadFile(sourceUrl, destFile) {
  console.log(sourceUrl, destFile);
  //console.log('download')
  request(sourceUrl).pipe(fs.createWriteStream(destFile));
}

module.exports = Post;