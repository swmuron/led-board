
// sync from cloud
var Post = require('../models/post');
var request = require('request');

var requestOptions = {
  url: 'http://hdepot.discord.us:9001/api/sync',
  rejectUnauthorized: false,
}

function cloudSync() {
  // request http from cloud
  request(requestOptions, function (error, response, body) {
    if (response) {
      console.log(body,response.statusCode,error);  
    }
    if (!error && response.statusCode == 200) {
      
      try {
        var bodyJson = JSON.parse(body);
        for (i in bodyJson) {
          Post.createFromCloud(bodyJson[i]);
        }
      } catch (e) {
        console.log(e);
      } finally {
        setTimeout(cloudSync, 20000);
      }
    } else {
      console.log(response.statusCode,error);
      setTimeout(cloudSync, 60000);
    }
  });
}

module.exports = function() {
  setTimeout(cloudSync, 15000);
}