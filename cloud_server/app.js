// Express modules
var express = require('express');

// Other modules
var config = require('./config/config');
var fs = require('fs');
var http = require('http');
var https = require('https');
// App, config, definition
var app = express();

// set up DB connection

// middleware
require('./middleware')(app);
// auto routers
require('./setupRoutes')(app);

// Listen to Twitter API stream
require('./lib/sources/twitter.js').streamPosts();
// Listen to Instagram API stream
require('./lib/sources/instagram').streamGrams(app);

// cron
//var Cron = require('./lib/cron/serverloop.js');
//var cronLoop = new Cron(socketServer);
//console.log('cronLoop',cronLoop.count);
//cronLoop.loop();

http.createServer(app).listen(config.HTTP.PORT);
/*
https.createServer({
  key: fs.readFileSync('config/server.key'),
  cert: fs.readFileSync('config/server.crt')
},app).listen(config.HTTP.PORT);
*/
console.log('Running with config',config.ENV_NAME)