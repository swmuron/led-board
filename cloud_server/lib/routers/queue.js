
var Post = require('../models/post.js'),
    express = require('express');

var QueueRouter = function() {
  var r = express.Router();
  
  r.get('/', function(req,res) {
    // get queue list
    //var pageKey = req.query.after || null;
    Post.getQueuedPost().then(function(data) {
      //console.log('len:',data.rows.length);
      return res.send(data.rows);
    }, function(err) {
      console.log('getqueue failed');
      return res.send(500);
    });
  });
  
  r.post('/accept', function(req, res) {
    //console.log(req.body);
    Post.updateStatus(req.body.id, 'accept').then(function(data) {
      res.send('ok');
    });
  });
  r.post('/reject', function(req, res) {
    // delete entirely?
    Post.updateStatus(req.body.id, 'reject').then(function(data) {
      res.send('ok');
    });
  });
  r.post('/ban', function(req, res) {
    //Post.updateStatus(req.body.id, 'ban');
    res.send(403);
  });

  return r;
}

module.exports = {
  path: 'queue',
  router: QueueRouter()
}