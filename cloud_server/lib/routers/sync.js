
var Post = require('../models/post.js'),
    express = require('express');

var SyncRouter = function() {
  var r = express.Router();
  
  r.get('/', function(req,res) {
    // get queue list
    //var pageKey = req.query.after || null;
    //console.log('getqueue');
    Post.getSync().then(function(data) {
      //console.log('got next display',data);
      res.send(data.rows);
      for (i in data.rows) {
        Post.setSynced(data.rows[i].id);
      }
      return;
    }, function(err) {
      console.log('sync failed',err);
      return res.send(500);
    });
  });
  /*
  r.get('/leaderboard', function(req,res) {
    //console.log('hit leaderboard')
    Post.getLeaderboard().then(function(rows) {
      //console.log('lboard',rows);
      return res.send(rows);
    })
  });
  */

  return r;
}

module.exports = {
  path: 'sync',
  router: SyncRouter()
}