// Post model helpers
// handle CRUD functions

var cfg = require('../../config/config.js');
//var q   = require('q');
var fs = require('fs');

var request = require('request');
var path = require('path'),
    appDir = path.dirname(require.main.filename);
var Cot = require('cot');

if (!global._db) {
  global._db = (new Cot({'port': cfg.DB.PORT, 'hostname': cfg.DB.HOST})).db(cfg.DB.NAME);
}

var Post = { };

var regexExt = /\.\w{3,4}$/gi;

var regexUrl = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/gi;
var regexTco = /http:\/\/t.co\/\w*/gi;

function stripURLs(str) {
  var i = str.replace(regexUrl,'');
  var ii = i.replace(regexTco,'');
  console.log(str,' > ',i,' >',ii);
  return ii;
}

Post.createFromTweet = function(src) {
  console.log('GOT TWEET',src.id_str);
  // map fields. prevents any weird stray field bugs
  var p = {}
  p._id = src.id_str;
  p.type = 'post';
  p.viewCount = 0;
  // _id is service-postid
  // maybe split based on service
  p.username     = src.user.screen_name.toLowerCase();
  p.text         = stripURLs(src.text);
  p.status = 'queued';
  var media = src.entities.media;
  if (media && media.length > 0) {
    // make sure media exists
    var m;
    for (var x = 0, xx = media.length; x < xx; x++) {
      m = media[x];
      if (m.type != 'photo' || !m.media_url) {
        continue; // reject non-photo media
      } else {
        // download that sucker
        var fileExt = regexExt.exec(m.media_url)[0];
        var filePath = '/downloads/twitter/'+p._id+fileExt;
        p.imageUrl = filePath;
        downloadFile(m.media_url, appDir+'/public'+filePath)
        
      }

    }
  }
  if (!p.imageUrl) {
    // didnt find a photo in entities, we'll pick at random
    // use a stock photo
    p.imageUrl = '/img/default.png'
  }

  // other data?

  Post.save(p);
  //console.log('SUCCESSFUL TWEET SAVE');
}

Post.createFromGram = function(src) {
  console.log('GOT GRAM',src.id);
  var p = {};
  p._id = src.id;
  p.username = src.user.username.toLowerCase();
  p.text = stripURLs(src.caption.text);
  p.status = 'queued';
  var extResult = regexExt.exec(src.images.low_resolution.url);
  var fileExt = null;
  if (extResult) {
    fileExt = extResult[0];
  } else {
    fileExt = '.jpg'; //wild guess
  }
  var filePath = '/downloads/instagram/'+p._id+fileExt;
  downloadFile(src.images.low_resolution, appDir+'/public'+filePath)
  p.imageUrl = filePath;
  Post.save(p);
  //console.log('SUCCESSFUL GRAM SAVE');
}

Post.getById = function(id) {
  return global._db.get(id);
}

Post.updateStatus = function(id, status) {
  // thus here, update the user leaderboard
  return global._db.update(id, function(doc) {
    doc.status = status;
    return doc;
  });
}

/* NOT USED ON CLOUD *

global.maxViewCount = 0;
global._db.view('post', 'display', {limit: 1, descending: true}).then(function(data) {
  global.maxViewCount = data.rows[0].key[0];
});
Post.updateViewCount = function(id) {
  return global._db.update(id, function(doc) {
    if (doc.viewCount+1 > global.maxViewCount) {
      global.maxViewCount = doc.viewCount+1;
    }
    doc.viewCount = global.maxViewCount;
    return doc;
  });
}
*/

Post.getQueuedPost = function(id) {
  return global._db.view('post', 'queue', {limit: 1});
}

Post.getSync = function() {
  return global._db.view('post', 'sync');
}

Post.setSynced = function(id) {
  console.log('setSynced',id);
  return global._db.update(id, function(doc) {
    doc.synced = true;
    return doc;
  });
}

/*
Post.getLeaderboard = function(id) {
  return global._db.view('post', 'leaderboard', {group: true}).then(function(data) {
    data.rows.sort(function(a, b) {
        return b.value - a.value;
    });

    return data.rows.slice(0,5);
  });
}
*/

Post.save = function(u) {
  return global._db.post(u); 
}

function downloadFile(sourceUrl, destFile) {
  //console.log(sourceUrl, destFile);
  request(sourceUrl).pipe(fs.createWriteStream(destFile));
}

module.exports = Post;