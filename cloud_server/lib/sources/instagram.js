
var cfg = require('../../config/config');

var express = require('express');
var Post = require('../models/post');
var ig = require('instagram-node').instagram();



ig.use({
  client_id: cfg.INSTAGRAM.CLIENT_ID,
  client_secret: cfg.INSTAGRAM.CLIENT_SECRET
});

var CALLBACK_URL = 'http://'+cfg.HTTP.HOST+':'+cfg.HTTP.PORT+'/callback/instagram/tag/'+cfg.INSTAGRAM.HASHTAG;

var MyInstagram = {};

function setupSubscription() {
  ig.add_tag_subscription(cfg.INSTAGRAM.HASHTAG, CALLBACK_URL, function(err, result, limit){
    console.log('added subscription',err,result,limit)
  });
}

var lastReadGram = 'none';

MyInstagram.streamGrams = function(app) {
  // setup app points
  var r = express.Router();
  
  r.get('/callback/instagram/tag/'+cfg.INSTAGRAM.HASHTAG, function(req,res) {
    //console.log('instagram sent something tagged',req.params.tag,req.query);
    console.log('received subscribe challenge',req.query['hub.challenge']);

    // instagram expects hub.challenge back
    res.send(req.query['hub.challenge']);
  });
  r.post('/callback/instagram/tag/'+cfg.INSTAGRAM.HASHTAG, function(req,res) {
    console.log('INSTAGRAM POST RECEIVED',req.body);
    // find recent tagged
    var limitTime = Date.now();
    for (var i in req.body) {
      if (req.body[i].time < limitTime) {
        limitTime = req.body[i].time;
      }
    }
    //console.log('limitTime',limitTime);
    ig.tag_media_recent(cfg.INSTAGRAM.HASHTAG, function(err, medias, pagination, limit) {
      //console.log(err,medias,pagination,limit);
      for (var x = 0, xx = medias.length; x < xx; x++) {
        var d = medias[x];
        console.log('x',x,d);
        if (d.id == lastReadGram || (lastReadGram == 'none' && x > 0)) {
          console.log('created_time before limit time',limitTime,d.created_time);
          limitTime = medias[0].created_time;
          lastReadGram = medias[0].id;
          break; // old gram
        }
        Post.createFromGram(d);
      }

    });
    res.send(req.query['hub.challenge']); // ack within 2 seconds
  });
  app.use(r);

  setupSubscription();
}

function handleGram(gram) {
  console.log('HANDLE GRAM',gram); 
}


module.exports = MyInstagram;