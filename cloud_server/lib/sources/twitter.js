

var cfg = require('../../config/config');

var Post = require('../models/post');

console.log('cfg',cfg.TWITTER);
var TwitterCore = require('twitter');
var Cot = require('cot');

var twitter = new TwitterCore({
    consumer_key: cfg.TWITTER.CONSUMER_KEY,
    consumer_secret: cfg.TWITTER.CONSUMER_SECRET,
    access_token_key: cfg.TWITTER.ACCESS_TOKEN_KEY, 
    access_token_secret: cfg.TWITTER.ACCESS_TOKEN_SECRET
  });

if (!global._db) {
  global._db = (new Cot({'port': cfg.DB.PORT, 'hostname': cfg.DB.HOST})).db(cfg.DB.NAME);
}

var MyTwitter = {};

MyTwitter.streamPosts = function() {
  // handle all the API stuff, call handler with just the post data and the source

  if (global._twitter_stream) {
    console.log('stream already active');
    return;
  }

  twitter.stream('statuses/filter', {track: cfg.TWITTER.HASHTAG, lang: 'en', filter_level: 'medium'}, function(strim) {
    strim.on('data', function(data) {
      console.log('got data',data);
      Post.createFromTweet(data);
    });
    strim.on('end', function() {
      console.log('strim end');
    });
    strim.on('error', function(err) {
      console.log('strim error',err,err.stack);
    });

    global._twitter_stream = strim;
    console.log('strim setup ok');
  });
}

module.exports = MyTwitter;