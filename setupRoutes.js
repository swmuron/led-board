var express = require('express'),
    fs      = require('fs');

module.exports = function(app) {
  // Automatically route anything in lib/routers
  var routers = fs.readdirSync('./lib/routers');

  var i,r;
  for (i = 0, ii = routers.length; i < ii; i++) {
    try {
      r = require('./lib/routers/'+routers[i]);
      app.use('/api/'+r.path, r.router);
    } catch (err) {
      console.log('while setting up',i,r,'\n',err.stack);
    }
  }
}